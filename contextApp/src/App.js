import React from 'react';
import Navbar from './components/Navbar'
import BookList from './components/BookList';
import ThemeContextProvider from './contexts/ThemeContext'
import ThemeToggle from './components/ThemeToggle'
import AuthContextProvider from './contexts/AuthContext'
import BookContexProvider from './contexts/BookContext';

function App() {
  return (
    <div className="App">
    <ThemeContextProvider>
      <AuthContextProvider>
        <Navbar />
        <BookContexProvider>
          <BookList />
        </BookContexProvider>
        <ThemeToggle />
      </AuthContextProvider>
    </ThemeContextProvider>
    </div>
  );
}

export default App;
